﻿using System;
using System.Collections.Generic;

namespace Task4
{
    class Program
    {
        //Create my Dictionary
        public static Dictionary<string, string> myDict = new Dictionary<string, string>();

        //Add contacts/people to the dictionary and get user input from the user for firstname and lastname of the contact
        static void myContacts()
        {
            myDict.Add("Irfan", "Nazand");
            myDict.Add("Manual", "Neuer");
            myDict.Add("Cristiano", "Ronaldo");
            myDict.Add("Robert", "Lewandowski");
            myDict.Add("Jerome", "Boateng");

            Console.Write("Who are you searching for? Please enter firstname: ");
            string firstName = Console.ReadLine();

            Console.Write("Please enter their lastname: ");
            string lastName = Console.ReadLine();

            SearchContact(firstName, lastName);
        }

        /**
         * Search contact method
         * Checks user input, and if input is valid and contact exist, returns the contact from the dictionary to console.
         * If user does not exist, returns message to user with information that user does not exist.
         **/
        static void SearchContact(string firstName, string lastName)
        {
            double noOneFound = 0;

            foreach (var person in myDict)
            {
                if (person.Key.ToLower().Equals(firstName.ToLower()) && person.Value.ToLower().Equals(lastName.ToLower()))
                {
                    Console.WriteLine("There was a person in our contacts: " + person);
                }

                else if (person.Key.ToLower().Contains(firstName.ToLower()))
                {
                    Console.WriteLine("There was a partial match for the Firstname you are searching for: " + person);
                }

                else if (person.Value.ToLower().Contains(lastName.ToLower()))
                {
                    Console.WriteLine("There was a partial match for the lastname you are searching for: " + person);
                }

                else
                {
                    noOneFound += 1;
                }
            }

            if ((myDict.Count / noOneFound) == 1)
            {
                Console.WriteLine("There was no matches with your search of firstname and lastname: " + firstName + " " + lastName);
            }
        }


        static int userPrompt()
        {
            try
            {
                int i = int.Parse(Console.ReadLine());
                return i;
            }
            catch (Exception)
            {
                Console.WriteLine("The number is not valid, please type a valid number: ");
                return userPrompt();
            }
        }

        static void DrawLine(int w, char ends, char mids)
        {
            Console.Write(ends);
            for (int i = 1; i < w - 1; ++i)
                Console.Write(mids);
                Console.WriteLine(ends);
        }

        static void DrawSquare()
        {
            Console.WriteLine("Please enter the Square size: ");
            int w = userPrompt();

            DrawLine(w, '*', '*');
            for (int i = 0; i < w - 1; ++i)
            {
                DrawLine(w, '*', ' ');
            }
            DrawLine(w, '*', '*');
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello, who are you looking for?");
            myContacts();
            DrawSquare();
        }
    }
}
